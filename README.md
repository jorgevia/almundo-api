# `Al Mundo API.`

### Descripción ###

* Este pequeño proyecto está realizado en node con Express js y Mongodb.

### Pasos para setear ambiente ###

1. Checkoutear repo
1. correr yarn
1. Records Mongo: Importar records en hotels.json a una base de datos => "almundo", colección => "hotels"
1. mongoimport --db almundo --collection hotels --file hotels.json 
1. (El archivo hotels.json se encuenta en el root del repositorio)
1. correr npm start para iniciar servidor, no olvidar tener corriendo el servicio mongod.
1. La api va a correr en en port 3000 (http://localhost:3000/) y en el port 27017 debería correr el servicio mongod

* Jorge Luis Viale
* viale.jorge@gmail.com