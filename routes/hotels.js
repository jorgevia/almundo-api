const express = require('express');
const router = express.Router();

// Mongo setup
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/almundo';
const assert = require('assert');
const collectionName = 'hotels';

/* GET hotels list */
router.get('/', (req, res, next) => {
    MongoClient.connect(url, (err, db) => {

        assert.equal(null, err);

        db.collection(collectionName).find()
            .sort(getParams())
            .toArray((err, docs) => {
            db.close();          
            res.send(docs);
        });

        function getParams() {
            let params = {};
            params[req.query.sortBy] = (req.query.sortCriteria === 'ASC' ? 1 : -1);
            return params;
        }
    });
});

module.exports = router;
